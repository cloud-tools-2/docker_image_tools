FROM ubuntu:18.04
LABEL maintainer="marek.marszelewski@wp.pl"

ARG TERRAFORM_VERSION=0.12.26
ARG TERRAGRUNT_VERSION=v0.23.5
ARG KAPP_VERSION=v0.27.0
ARG KUSTOMIZE_VERSION=v3.5.3
ARG SOPS_VERSION=v3.6.0

ENV TZ=Europe/Warsaw

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone

RUN apt update -y && \
    apt-get update -y && \
    apt install -y python && \
    apt install -y python3-pip && \
    apt-get install -y apt-transport-https && \
    apt-get install -y curl wget tar unzip && \
    apt-get install -y gnupg2 && \
    apt-get install -y git && \
    apt-get install -y locales && \
    apt-get install -y build-essential && \
    apt-get install -y apt-transport-https ca-certificates gnupg && \
    apt-get install -y software-properties-common

RUN add-apt-repository -y ppa:longsleep/golang-backports && \
    apt install -y golang-go

RUN pip3 install awscli
RUN localedef -i en_US -f UTF-8 en_US.UTF-8

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl

RUN wget https://github.com/k14s/kapp/releases/download/${KAPP_VERSION}/kapp-linux-amd64 && \
    chmod +x kapp-linux-amd64 && \
    mv kapp-linux-amd64 /usr/local/bin/kapp

RUN wget https://github.com/gruntwork-io/terragrunt/releases/download/${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 && \
    chmod +x terragrunt_linux_amd64 && \
    mv terragrunt_linux_amd64 /usr/local/bin/terragrunt

RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    chmod +x terraform && \
    mv terraform /usr/local/bin/

RUN wget https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
    tar -zxf kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
    chmod +x kustomize && \
    mv kustomize /usr/local/bin/

RUN echo "deb http://packages.cloud.google.com/apt cloud-sdk-stretch main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && apt-get install google-cloud-sdk -y

RUN wget https://github.com/mozilla/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux && \
    mv sops-${SOPS_VERSION}.linux /usr/local/bin/sops && \
    chmod +x /usr/local/bin/sops

COPY root/ /

RUN kapp --version
RUN terragrunt --version
RUN terraform --version
RUN kustomize version
RUN go version
RUN sops --version
