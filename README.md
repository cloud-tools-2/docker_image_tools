### **Docker Image Tools**

The image is based on ubuntu: 18.04 and contains:
* Terraform 
* Terragrunt
* Kapp 
* Kustomize 
* Kubectl
* Golang
* AWS CLI
* GCP
* SOPS

The most recent changes are always on the master and tagged.

**! IMPORTANT !**

All changes made to the repository must be described and included in the CHANGELOG.md file.

# Instructions for making changes

1. Download the latest changes from the master and create a feature branch where we make the changes.
2. Push changes to the feature branch and then create a merge request to the master.
3. Merge changes to the master.
4. After merging the changes to the master, edit the CHANGELOG.md file on the master and push the changes made by entering commit "Update CHANGELOG.md".
5. After making changes to the CHANGELOG.md file, create a tag corresponding to the version contained in the CHANGELOG.md file.
6. After creating a tag, the version specified in the tag (and latest) will be built and pushed.
